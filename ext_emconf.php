<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Extbase Session Entities',
    'description' => 'Extends TYPO3\'s Extbase framework by possibility to work with session entities over several actions without persisting, including partial model validation.',
    'category' => 'plugin',
    'author' => '',
    'author_email' => '',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.7.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'T3\\ExtbaseSessionEntities\\' => 'Classes'
        ]
    ]
];
