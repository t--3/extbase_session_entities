<?php
namespace T3\ExtbaseSessionEntities\ViewHelpers;

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * Class FormViewHelper
 */
class FormViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\FormViewHelper
{
    /**
     * Renders a hidden form field containing the technical identity of the given object.
     *
     * @param object $object Object to create the identity field for
     * @param string $name Name
     * @return string A hidden field containing the Identity (uid in Extbase) of the given object or NULL
     * @see \TYPO3\CMS\Extbase\Mvc\Controller\Argument::setValue()
     */
    protected function renderHiddenIdentityField($object, $name)
    {
        if (!$object instanceof \T3\ExtbaseSessionEntities\Mvc\AbstractSessionEntity) {
            return parent::renderHiddenIdentityField($object, $name);
        }

        // Intentionally NOT using PersistenceManager::getIdentifierByObject here!!
        // Using that one breaks re-submission of data in forms in case of an error.
        $identifier = $object->getUid();
        if ($identifier === null) {
            return LF . '<!-- Object of type ' . get_class($object) . ' is without identity -->' . LF;
        }
        $name = $this->prefixFieldName($name) . '[__identity]';
        $this->registerFieldNameForFormTokenGeneration($name);

        return LF . '<input type="hidden" name="' . $name . '" value="' . $identifier . '" />' . LF;
    }

    /**
     * Extend the trusted fields by those, which are already filled in session entity.
     * Property mapper would complain on errorAction, that unknown properties will not get mapped,
     * because it does not update the internal arguments (containing the trusted properties) of the request.
     *
     * @return string The hmac field
     */
    protected function renderTrustedPropertiesField()
    {
        $formFieldNames = $this->viewHelperVariableContainer->get(\TYPO3\CMS\Fluid\ViewHelpers\FormViewHelper::class, 'formFieldNames');

        /** @var \T3\ExtbaseSessionEntities\Mvc\AbstractSessionEntity $object */
        $object = $this->arguments['object'];
        if ($object instanceof \T3\ExtbaseSessionEntities\Mvc\AbstractSessionEntity) {

            $formFieldPrefix = '';
            foreach ($formFieldNames as $formFieldName) {
                if (!empty($formFieldName)) {
                    $formFieldPrefix = preg_replace('/(.*?)\[.*/', '$1', $formFieldName);
                    break;
                }
            }

            $ignoreProperties = ['fromSession', 'pid', 'sessionIdentifier', 'uid'];
            $properties = \TYPO3\CMS\Extbase\Reflection\ObjectAccess::getGettableProperties($object);
            foreach ($properties as $name => $value) {
                if (!in_array($name, $ignoreProperties) && !empty($value)) {
                    $formFieldNames[] = $formFieldPrefix . '[' . $name . ']';
                }
            }
        }

        $requestHash = $this->mvcPropertyMappingConfigurationService->generateTrustedPropertiesToken($formFieldNames, $this->getFieldNamePrefix());
        return '<input type="hidden" name="' . $this->prefixFieldName('__trustedProperties') . '" value="' . htmlspecialchars($requestHash) . '" />';
    }
}
