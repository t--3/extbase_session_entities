<?php
namespace T3\ExtbaseSessionEntities\Mvc;

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * SessionRepository
 */
class SessionRepository implements \TYPO3\CMS\Core\SingletonInterface
{
    const PREFIX = 'entity_';

    /**
     * @var \TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication
     */
    protected $frontendUser;

    /**
     * SessionRepository constructor
     */
    public function __construct()
    {
        $this->frontendUser = $GLOBALS['TSFE']->fe_user;
    }

    /**
     * Get entity from session by given identifier
     *
     * @param string $identifier
     * @return AbstractSessionEntity|bool Returns false if identifier is null or not found in session
     */
    public function get(string $identifier = null)
    {
        if (is_null($identifier)) {
            return false;
        }
        return unserialize($this->frontendUser->getKey('ses', self::PREFIX . $identifier));
    }

    /**
     * Stores given entity to session
     *
     * @param AbstractSessionEntity $sessionEntity
     * @return void
     */
    public function set(AbstractSessionEntity $sessionEntity)
    {
        $this->frontendUser->setKey(
            'ses',
            self::PREFIX . $sessionEntity->getSessionIdentifier(),
            serialize($sessionEntity)
        );
    }

    /**
     * Removes given entity from session
     *
     * @param AbstractSessionEntity $sessionEntity
     * @return void
     */
    public function remove(AbstractSessionEntity $sessionEntity)
    {
        $this->frontendUser->setKey('ses', self::PREFIX . $sessionEntity->getSessionIdentifier(), null);
    }
}
