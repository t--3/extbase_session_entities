<?php
namespace T3\ExtbaseSessionEntities\Mvc;

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */
use TYPO3\CMS\Extbase\Validation\Validator\ConjunctionValidator;

/**
 * SessionRepository
 */
class ValidatorResolver extends \TYPO3\CMS\Extbase\Validation\ValidatorResolver
{
    /**
     * Returns empty base validator for AbstractSesssionEntities
     *
     * @param string $targetClassName The data type to search a validator for. Usually the fully qualified object name
     * @return ConjunctionValidator The validator conjunction or NULL
     */
    public function getBaseValidatorConjunction($targetClassName)
    {
        if (is_subclass_of($targetClassName, AbstractSessionEntity::class)) {
            /** @var ConjunctionValidator $conjunctionValidator */
            $conjunctionValidator = $this->objectManager->get(ConjunctionValidator::class);
            return $conjunctionValidator;
        }
        return parent::getBaseValidatorConjunction($targetClassName);
    }
}
