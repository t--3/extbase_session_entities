<?php
namespace T3\ExtbaseSessionEntities\Mvc;

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */
use TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface;

/**
 * XClass of Extbase's Property Mapper
 */
class PropertyMapper extends \TYPO3\CMS\Extbase\Property\PropertyMapper
{
    /**
     * Map $source to $targetType, and return the result
     *
     * Modifies mapping configuration to just skip unknown properties
     * This may happen, when session entities cause an error action call
     *
     * @param mixed $source the source data to map. MUST be a simple type, NO object allowed!
     * @param string $targetType The type of the target; can be either a class name or a simple type.
     * @param PropertyMappingConfigurationInterface $configuration Configuration for the property mapping.
     * @return mixed an instance of $targetType
     */
    public function convert($source, $targetType, PropertyMappingConfigurationInterface $configuration = null)
    {
        if (is_subclass_of($targetType, AbstractSessionEntity::class) &&
            isset($source['__identity'])
        ) {
            $configuration->skipUnknownProperties();
        }
        return parent::convert($source, $targetType, $configuration);
    }
}
