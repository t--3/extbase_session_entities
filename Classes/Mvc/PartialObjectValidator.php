<?php
namespace T3\ExtbaseSessionEntities\Mvc;

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Class PartialObjectValidator
 */
class PartialObjectValidator extends \TYPO3\CMS\Extbase\Validation\Validator\GenericObjectValidator
{
    /**
     * @var array
     */
    protected $supportedOptions = [
        'validate' => ['*', 'Comma separated list of attributes to validate', 'string']
    ];

    /**
     * @param mixed $value
     * @return \TYPO3\CMS\Extbase\Error\Result
     */
    public function validate($value)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var \TYPO3\CMS\Extbase\Validation\ValidatorResolver $resolver */
        $resolver = $objectManager->get(\TYPO3\CMS\Extbase\Validation\ValidatorResolver::class);

        $attributesToValidate = GeneralUtility::trimExplode(',', $this->options['validate'], true);

        $validator = $resolver->getBaseValidatorConjunction(get_class($value));
        foreach ($validator->getValidators() as $validator) {
            if ($validator instanceof \TYPO3\CMS\Extbase\Validation\Validator\GenericObjectValidator) {
                $newPropertyValidators = [];
                foreach ($validator->getPropertyValidators() as $propertyName => $propertyValidator) {
                    if (in_array($propertyName, $attributesToValidate) || $this->options['validate'] === '*') {
                        $newPropertyValidators[$propertyName] = $propertyValidator;
                    }
                }
                $validator->propertyValidators = $newPropertyValidators;
            }
        }

        $result = $validator->validate($value);
        return $result;
    }
}
