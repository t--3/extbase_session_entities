<?php
namespace T3\ExtbaseSessionEntities\Mvc;

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Class AbstractPartialEntityController
 */
class AbstractSessionEntityController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var \T3\ExtbaseSessionEntities\Mvc\SessionRepository
     * @inject
     */
    protected $sessionRepository;

    /**
     * @var \T3\ExtbaseSessionEntities\Mvc\ValidatorResolver
     * @inject
     */
    protected $validatorResolver;

    /**
     * Detect status of entity (session or database) and handles persistence.
     *
     * @param AbstractSessionEntity $entity
     * @param Repository|null $repo
     */
    protected function updateSessionEntity(AbstractSessionEntity $entity, Repository $repo = null)
    {
        if ($entity->isFromSession()) {
            $this->sessionRepository->set($entity);
        } elseif (!$this->request->getOriginalRequestMappingResults() ||
                  !$this->request->getOriginalRequestMappingResults()->hasErrors()
        ) {
            if (!$repo) {
                throw new \InvalidArgumentException(
                    'Can\'t update database entity, without given repository instance.'
                );
            }
            $repo->add($entity);
        }
    }

    /**
     * Helper function to perform a persistAll
     *
     * @return void
     */
    protected function persistAll()
    {
        $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class)->persistAll();
    }
}
