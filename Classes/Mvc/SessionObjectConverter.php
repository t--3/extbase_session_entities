<?php
namespace T3\ExtbaseSessionEntities\Mvc;

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */

class SessionObjectConverter extends \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter
{
    /**
     * @var \T3\ExtbaseSessionEntities\Mvc\SessionRepository
     * @inject
     */
    protected $session;

    /**
     * @var int Higher priority than PersistentObjectConverter
     */
    protected $priority = 30;

    /**
     * We can only convert if the $targetType is subclass of AbstractSessionEntity.
     *
     * @param mixed $source
     * @param string $targetType
     * @return bool
     */
    public function canConvertFrom($source, $targetType)
    {
        return is_subclass_of($targetType, \T3\ExtbaseSessionEntities\Mvc\AbstractSessionEntity::class);
    }

    /**
     * Fetch an object from user session.
     *
     * @param mixed $identity
     * @param string $targetType
     * @return object
     */
    protected function fetchObjectFromPersistence($identity, $targetType)
    {
        if (ctype_digit((string)$identity)) {
            return parent::fetchObjectFromPersistence($identity, $targetType);
        }
        return $this->session->get($identity);
    }


    /**
     * Convert an object from $source to an entity or a value object.
     *
     * @param mixed $source
     * @param string $targetType
     * @param array $convertedChildProperties
     * @param \TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface $configuration
     * @return object the target type
     */
    public function convertFrom(
        $source,
        $targetType,
        array $convertedChildProperties = [],
        \TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface $configuration = null
    ) {
        $object = null;
        if (is_array($source)) {
            $object = $this->getSessionObjectBySource(
                $source,
                $targetType,
                $convertedChildProperties,
                $configuration
            );
        } elseif (is_string($source)) {
            if (ctype_digit($source)) {
                // TODO: Early return
                return parent::convertFrom($source, $targetType, $convertedChildProperties, $configuration);
            }
            $object = $this->session->get($source);
        }

        foreach ($convertedChildProperties as $propertyName => $propertyValue) {
            $result = \TYPO3\CMS\Extbase\Reflection\ObjectAccess::setProperty($object, $propertyName, $propertyValue);
            if ($result === false) {
                $exceptionMessage = sprintf(
                    'Property "%s" having a value of type "%s" could not be set in target object of type "%s". ' . 'Make sure that the property is accessible properly, for example via an appropriate setter method.',
                    $propertyName,
                    (is_object($propertyValue) ? get_class($propertyValue) : gettype($propertyValue)),
                    $targetType
                );
                throw new \TYPO3\CMS\Extbase\Property\Exception\InvalidTargetException($exceptionMessage, 1297935345);
            }
        }
        return $object;
    }

    /**
     * @param $source
     * @param $targetType
     * @param array $convertedChildProperties
     * @param \TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface $configuration
     * @return AbstractSessionEntity
     * @throws \TYPO3\CMS\Extbase\Property\Exception\InvalidTargetException
     */
    protected function getSessionObjectBySource(
        $source,
        $targetType,
        array $convertedChildProperties,
        \TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface $configuration
    ) : AbstractSessionEntity {

        if (isset($source['__identity']) && !ctype_digit($source['__identity'])) {
            $object = $this->session->get($source['__identity']);
        } else {
            $object = parent::convertFrom($source, $targetType, $convertedChildProperties, $configuration);
        }

        if (!$object) {
            /** @var AbstractSessionEntity $object */
            $object = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance($targetType);
            $object->setSessionIdentifier($source['__identity']);

            foreach ($source as $propertyName => $propertyValue) {
                if ($propertyName === '__identity') {
                    $propertyName = 'uid';
                }
                \TYPO3\CMS\Extbase\Reflection\ObjectAccess::setProperty($object, $propertyName, $propertyValue);
            }
        }
        return $object;
    }
}
