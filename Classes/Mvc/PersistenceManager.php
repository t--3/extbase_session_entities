<?php
namespace T3\ExtbaseSessionEntities\Mvc;

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * XClass of Extbase's PersistenceManager
 * to support session objects
 *
 * @package T3\ExtbaseSessionEntities\Mvc
 */
class PersistenceManager extends \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
{
    /**
     * Adds an object to the persistence.
     * If is instance of AbstractSessionEntity
     *
     * @param object $object The object to add
     */
    public function add($object)
    {
        if ($object instanceof AbstractSessionEntity && $object->isFromSession()) {
            $object->__finalize();
        }
        parent::add($object);
    }
}
