<?php
namespace T3\ExtbaseSessionEntities\Mvc;

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * Class AbstractSessionEntity
 */
abstract class AbstractSessionEntity extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * @var string
     */
    protected $sessionIdentifier = '';

    /**
     * Prepares session entity. It is required to call this function
     * before passing entity to form view helper.
     *
     * @return self
     */
    public function prepare()
    {
        $this->setSessionIdentifier();
        return $this;
    }

    /**
     * Returns the generated session identifier
     *
     * @return string
     */
    public function getSessionIdentifier() : string
    {
        return $this->sessionIdentifier;
    }

    /**
     * Checks if the set uid is numeric. If not, it's from session.
     *
     * @return bool True when this entity is not stored in database yet
     */
    public function isFromSession() : bool
    {
        return !ctype_digit((string) $this->uid);
    }

    /**
     * Removes this entity from session and cleans it up for persistence
     * Used in x-classed PersistenceManager
     *
     * @return self
     * @internal
     */
    public function __finalize()
    {
        $this->uid = null;

        /** @var SessionRepository $sessionRepository */
        $sessionRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(SessionRepository::class);
        $sessionRepository->remove($this);

        foreach (\TYPO3\CMS\Extbase\Reflection\ObjectAccess::getGettableProperties($this) as $propertyValue) {
            if ($propertyValue instanceof \TYPO3\CMS\Extbase\Persistence\ObjectStorage) {
                /** @var AbstractSessionEntity $object */
                foreach ($propertyValue as $object) {
                    $object->__finalize();
                }
            }
        }
        return $this;
    }

    /**
     * Handles the session identifier
     *
     * @param string|null $sessionIdentifier
     * @return string
     * @internal Use prepare() method instead
     */
    public function setSessionIdentifier(string $sessionIdentifier = null) : string
    {
        if (!$this->sessionIdentifier && is_null($this->uid)) {
            $id = $sessionIdentifier ?: 's' . uniqid();
            $this->sessionIdentifier = $id;
            $this->uid = $id;
        }
        return $this->sessionIdentifier;
    }

    /**
     * Returns session identifier if uid is not set yet
     *
     * @return int|string
     */
    public function getUid()
    {
        return $this->uid;
    }
}
