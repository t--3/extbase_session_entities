<?php
defined('TYPO3_MODE') || die('Access denied.');

/*  | This extension is made with love for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018 Armin Vieweg <armin@v.ieweg.de>
 */

call_user_func(
    function () {
        // Register Type Converter
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter(
            \T3\ExtbaseSessionEntities\Mvc\SessionObjectConverter::class
        );

        // Register XClasses
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class]
            = ['className' => \T3\ExtbaseSessionEntities\Mvc\PersistenceManager::class];
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extbase\Property\PropertyMapper::class]
            = ['className' => \T3\ExtbaseSessionEntities\Mvc\PropertyMapper::class];
    }
);
